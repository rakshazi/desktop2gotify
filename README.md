# Desktop notifications to Gotify, Matrix or Webhook

[![coverage report](https://gitlab.com/rakshazi/desktop2gotify/badges/master/coverage.svg)](https://gitlab.com/rakshazi/desktop2gotify/-/commits/master)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)

That tool will read your desktop notifications with DBus and send them to your gotify server or webhook url or matrix.org HomeServer

### Usage

> **NOTE**: you don't need to set all arguments, use only required, eg: if you want to use gotify, set `--gotify-` prefixied args

```bash
$ desktop2gotify --help
Usage of desktop2gotify:
  --debug
        Debug log
  --gotify-token string
        Gotify Application Token
  --gotify-url string
        Gotify API url (default "http://localhost")
  --ignore string
        List of apps to ignore, divided by comma, eg: chrome,firefox,slack
  --matrix-hs string
        Matrix.org homeserver (default "https://matrix.org")
  --matrix-room string
        Matrix room id or alias, eg: #myroom:matrix.org
  --matrix-token string
        Matrix access token
  --matrix-user string
        Matrix user, eg: @user:matrix.org
  --webhook-url string
        Webhook API url
```

Example:

```bash
desktop2gotify --ignore chromium,hubstaff --gotify-url http://localhost --gotify-token Xz4KiNRROztPCOi
```

## Run

### Golang

```bash
go install gitlab.com/rakshazi/desktop2gotify
desktop2gotify --help
```

### Binary

```bash
curl https://gitlab.com/rakshazi/desktop2gotify/-/jobs/artifacts/master/raw/app?job=binary --output desktop2gotify
chmod +x ./desktop2gotify
./desktop2gotify
```

### systemd

1. Create file `~/.config/systemd/user/desktop2gotify.service` (may be you need to create dirs too, varies by linux distro)
2. Copy [systemd.service](./systemd.service) content to that file
3. Replace `/path/to/desktop2gotify` to the path of your desktop2gotify binary
4. Replace `PUT_YOUR_ARGUMENTS_HERE` with your configuration (check `desktop2gotify -h` for all available options)
5. Run `systemctl --user daemon-reload; systemctl --user enable desktop2gotify; systemctl --user start desktop2gotify`
6. Check logs with `journalctl --user -u desktop2gotify`
