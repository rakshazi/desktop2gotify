package dbus

import (
	"fmt"
	"os"
	"strings"

	dbus "github.com/guelfey/go.dbus"
	"gitlab.com/rakshazi/desktop2gotify/gotify"
)

// DBus notifications channel
var notifications = make(chan *dbus.Message, 10)

// ReadNotifications - Connect to dbus and start reading notifications
func ReadNotifications(output chan *gotify.Message, ignored []string, debug bool) {
	conn, err := dbus.SessionBus()
	if err != nil {
		fmt.Println("[dbus]", err)
		os.Exit(1)
	}
	conn.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "eavesdrop='true',interface='org.freedesktop.Notifications',member='Notify'")
	conn.Eavesdrop(notifications)
	for notification := range notifications {
		if debug {
			fmt.Println("[dbus] notification", notification.Body)
		}
		if !IsIgnored(strings.ToLower(notification.Body[0].(string)), ignored) {
			if debug {
				fmt.Println("[dbus] notification is NOT ignored, seding it", notification.Body)
			}
			output <- CreateMessage(notification.Body[0], notification.Body[3], notification.Body[4])
		}
	}
}

// CreateMessage from dbus notification
func CreateMessage(application, title, body interface{}) *gotify.Message {
	text := title.(string) + " - " + body.(string)

	message := &gotify.Message{
		Title:    application.(string),
		Message:  text,
		Priority: 10,
	}

	return message

}

// IsIgnored - check if application is ignored
func IsIgnored(application string, ignored []string) bool {
	for _, ignore := range ignored {
		if application == ignore {
			return true
		}
	}
	return false
}
