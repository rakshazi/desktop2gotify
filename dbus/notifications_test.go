package dbus

import (
	"testing"
)

func TestCreateMessage(t *testing.T) {
	tables := []struct {
		Application string
		Title       string
		Body        string
	}{
		{"go-test1", "Title", "Body"},
		{"go-test2", "", "Body"},
		{"go-test3", "", ""},
	}

	for _, table := range tables {
		t.Run(table.Application, func(t *testing.T) {
			text := table.Title + " - " + table.Body
			message := CreateMessage(table.Application, table.Title, table.Body)
			if message.Title != table.Application {
				t.Errorf("Message title '%s' is incorrect. Expected: '%s'", message.Title, table.Application)
			}
			if message.Message != text {
				t.Errorf("Message text '%s' is incorrect. Expected: '%s'", message.Message, text)
			}
		})
	}
}

func TestIsIgnored(t *testing.T) {
	tables := []struct {
		Application string
		Ignored     []string
	}{
		{"chrome", []string{"chrome", "firefox", "safari"}},
		{"edge", []string{"chrome", "firefox", "safari"}},
		{"firefox", []string{}},
	}

	for _, table := range tables {
		t.Run(table.Application, func(t *testing.T) {
			shouldBeIgnored := false
			ignored := IsIgnored(table.Application, table.Ignored)
			for _, app := range table.Ignored {
				if table.Application == app {
					shouldBeIgnored = true
				}
			}
			if ignored != shouldBeIgnored {
				t.Fail()
			}
		})
	}
}
