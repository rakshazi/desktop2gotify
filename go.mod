module gitlab.com/rakshazi/desktop2gotify

go 1.14

require (
	github.com/guelfey/go.dbus v0.0.0-20131113121618-f6a3a2366cc3
	github.com/matrix-org/gomatrix v0.0.0-20200827122206-7dd5e2a05bcd
)
