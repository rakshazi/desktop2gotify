package gotify

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Gotify API config
type Gotify struct {
	URL    string
	Token  string
	Client *http.Client
}

// Message to send
type Message struct {
	Title    string `json:"title"`
	Message  string `json:"message,omitempty"`
	Priority int    `json:"priority,omitempty"`
}

// New Gotify client
func New(url, token string) *Gotify {
	return &Gotify{URL: url, Token: token, Client: &http.Client{}}
}

// Send new message
func (api *Gotify) Send(message *Message, debug bool) error {
	messageBytes, err := json.Marshal(message)
	if err != nil {
		fmt.Println("[gotify]", err)
		return err
	}
	if debug {
		fmt.Println("[gotify] POST "+api.URL+"/message", message)
	}
	req, err := http.NewRequest("POST", api.URL+"/message", bytes.NewReader(messageBytes))
	if err != nil {
		fmt.Println("[gotify]", err)
		return err
	}
	req.Header.Set("X-Gotify-Key", api.Token)
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.Client.Do(req)
	if err != nil {
		fmt.Println("[gotify]", err)
		return err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != 200 {
		fmt.Println("[gotify]", string(body))
		return fmt.Errorf(string(body))
	}

	return nil
}

// Health - check
func (api *Gotify) Health() bool {
	req, err := http.NewRequest("GET", api.URL+"/health", nil)
	if err != nil {
		fmt.Println("[gotify]", err)
		return false
	}
	resp, err := api.Client.Do(req)
	if err != nil {
		fmt.Println("[gotify]", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("[gotify] status", resp.StatusCode)
		return false
	}
	return true
}
