package gotify

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	uri := "https://non.existing.endpoint"
	api := New(uri, "")
	if api.URL != uri {
		t.Fail()
	}
}

func TestSend(t *testing.T) {
	sentMessage := &Message{
		Title:    "Test",
		Message:  "Test",
		Priority: 0,
	}
	receivedMessage := Message{}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
		err = json.Unmarshal(body, &receivedMessage)
		if err != nil {
			t.Error(err)
		}
		if !reflect.DeepEqual(*sentMessage, receivedMessage) {
			t.Errorf("Received message is not the same as sent message")
		}
		fmt.Fprintln(w, "")
	}))
	defer server.Close()
	api := New(server.URL, "")
	err := api.Send(sentMessage, true)
	if err != nil {
		t.Error(err)
	}
}

func TestHealth(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "")
	}))
	defer server.Close()
	api := New(server.URL, "")
	if !api.Health() {
		t.Fail()
	}
}
