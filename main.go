package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/rakshazi/desktop2gotify/dbus"
	"gitlab.com/rakshazi/desktop2gotify/gotify"
	"gitlab.com/rakshazi/desktop2gotify/matrix"
	"gitlab.com/rakshazi/desktop2gotify/webhook"
)

var api *gotify.Gotify
var wh *webhook.Webhook
var mx *matrix.Matrix
var messages = make(chan *gotify.Message, 10)
var ignored = ""
var debug = false
var gotifyURL string
var webhookURL string
var gotifyToken string
var matrixHS string
var matrixUser string
var matrixToken string
var matrixRoom string

func init() {
	// app flags
	flag.BoolVar(&debug, "debug", false, "Debug log")
	flag.StringVar(&ignored, "ignore", "", "List of apps to ignore, divided by comma, eg: chrome,firefox,slack")

	// gotify flags
	flag.StringVar(&gotifyURL, "gotify-url", "http://localhost", "Gotify API url")
	flag.StringVar(&gotifyToken, "gotify-token", "", "Gotify Application Token")

	// webhook flags
	flag.StringVar(&webhookURL, "webhook-url", "", "Webhook API url")

	// matrix flags
	flag.StringVar(&matrixHS, "matrix-hs", "https://matrix.org", "Matrix.org homeserver")
	flag.StringVar(&matrixRoom, "matrix-room", "", "Matrix room id or alias, eg: #myroom:matrix.org")
	flag.StringVar(&matrixUser, "matrix-user", "", "Matrix user, eg: @user:matrix.org")
	flag.StringVar(&matrixToken, "matrix-token", "", "Matrix access token")
}

func initBackends() {
	if gotifyURL != "" && gotifyToken != "" {
		api = gotify.New(gotifyURL, gotifyToken)
		if !api.Health() {
			fmt.Println("Gotify server is unhealthy, check it first")
			os.Exit(1)
		}
	}
	if webhookURL != "" {
		wh = webhook.New(webhookURL)
	}

	if matrixToken != "" {
		mx = matrix.New(matrixHS, matrixUser, matrixToken, matrixRoom)
	}

	if api == nil && wh == nil && mx == nil {
		fmt.Println("You must set gotify API url and token OR webhook url OR matrix config, check --help")
		os.Exit(1)
	}
}

func main() {
	flag.Parse()
	initBackends()
	ignore := GetIgnored()

	go dbus.ReadNotifications(messages, ignore, debug)
	fmt.Println("[desktop2gotify] running")
	for message := range messages {
		if api != nil {
			api.Send(message, debug)
		}
		if wh != nil {
			wh.Send(message, debug)
		}
		if mx != nil {
			mx.Send(message, debug)
		}
	}
}

// GetIgnored apps
func GetIgnored() []string {
	items := strings.Split(ignored, ",")
	for i, item := range items {
		items[i] = strings.ToLower(strings.TrimSpace(item))
	}
	if debug {
		fmt.Println("[main] ignore", items)
	}

	return items
}
