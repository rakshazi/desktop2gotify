package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestInitBackends(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "")
	}))
	defer server.Close()
	webhookURL = server.URL
	gotifyURL = server.URL
	gotifyToken = server.URL // just for testing
	initBackends()
	if wh == nil || wh.URL != webhookURL {
		t.Errorf("webhook was not initialized correctly")
	}
	if api == nil || api.URL != gotifyURL {
		t.Errorf("gotify was not initialized correctly")
	}
}

func TestGetIgnored(t *testing.T) {
	debug = true
	ignored = "chrome,firefox,safari"
	expected := []string{"chrome", "firefox", "safari"}
	slice := GetIgnored()
	if !reflect.DeepEqual(slice, expected) {
		t.Fail()
	}
}
