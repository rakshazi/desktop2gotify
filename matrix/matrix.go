package matrix

import (
	"fmt"

	"github.com/matrix-org/gomatrix"
	"gitlab.com/rakshazi/desktop2gotify/gotify"
)

// Matrix bot config
type Matrix struct {
	HomeServer string
	User       string
	Token      string
	Room       string
	Client     *gomatrix.Client
}

// New matrix client
func New(hs string, user string, token string, room string) *Matrix {
	client, err := gomatrix.NewClient(hs, user, token)
	if err != nil {
		fmt.Println("[matrix] cannot start client, check your credentials")
		panic(err)
	}
	err = client.SetStatus("online", "reading notifications")
	if err != nil {
		fmt.Println("[matrix] cannot set status")
		panic(err)
	}
	_, err = client.JoinRoom(room, "", nil)
	if err != nil {
		fmt.Println("[matrix] cannot join room, did you invite that user to room?")
		panic(err)
	}

	return &Matrix{
		HomeServer: hs,
		User:       user,
		Token:      token,
		Room:       room,
		Client:     client,
	}
}

// Send new message
func (mx *Matrix) Send(gotifyMessage *gotify.Message, debug bool) error {
	message := gotifyMessage.Title + gotifyMessage.Message
	formattedMessage := "<b>" + gotifyMessage.Title + "</b> " + gotifyMessage.Message
	_, err := mx.Client.SendFormattedText(mx.Room, message, formattedMessage)
	if err != nil {
		fmt.Println("[matrix] cannot send message", err)
		return err
	}

	return nil
}
