package matrix

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/rakshazi/desktop2gotify/gotify"
)

func TestNew(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "{}")
	}))
	defer server.Close()
	mx := New(server.URL, "@test:test.test", "test", "#test:test.test")
	if mx == nil {
		t.Fail()
	}
}

func TestSend(t *testing.T) {
	sentMessage := &gotify.Message{
		Title:    "Test",
		Message:  "Test",
		Priority: 0,
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "{}")
	}))
	defer server.Close()
	mx := New(server.URL, "@test:test.test", "test", "#test:test.test")
	if mx == nil {
		t.Fail()
	}
	err := mx.Send(sentMessage, true)
	if err != nil {
		t.Error(err)
	}
}
