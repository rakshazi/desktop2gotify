package webhook

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/rakshazi/desktop2gotify/gotify"
)

// Webhook config
type Webhook struct {
	URL    string
	Client *http.Client
}

// Message to send.
type message struct {
	User   string `json:"displayName,omitempty"`
	Text   string `json:"text,omitempty"`
	Format string `json:"format,omitempty"`
	Avatar string `json:"avatarUrl,omitempty"`
}

// New Webhook client
func New(url string) *Webhook {
	return &Webhook{URL: url, Client: &http.Client{}}
}

// Send new message
func (wh *Webhook) Send(gotifyMessage *gotify.Message, debug bool) error {
	whMessage := &message{
		User:   gotifyMessage.Title,
		Text:   gotifyMessage.Message,
		Format: "plain",
	}
	messageBytes, err := json.Marshal(whMessage)
	if err != nil {
		fmt.Println("[webhook]", err)
		return err
	}
	if debug {
		fmt.Println("[webhook] POST "+wh.URL, whMessage)
	}
	req, err := http.NewRequest("POST", wh.URL, bytes.NewReader(messageBytes))
	if err != nil {
		fmt.Println("[webhook]", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := wh.Client.Do(req)
	if err != nil {
		fmt.Println("[webhook]", err)
		return err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != 200 {
		fmt.Println("[webhook]", string(body))
		return fmt.Errorf(string(body))
	}

	return nil
}
