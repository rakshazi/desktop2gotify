package webhook

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/rakshazi/desktop2gotify/gotify"
)

func TestNew(t *testing.T) {
	uri := "https://non.existing.endpoint"
	wh := New(uri)
	if wh.URL != uri {
		t.Fail()
	}
}

func TestSend(t *testing.T) {
	sentMessage := &gotify.Message{
		Title:    "Test",
		Message:  "Test",
		Priority: 0,
	}
	receivedMessage := message{}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Error(err)
		}
		err = json.Unmarshal(body, &receivedMessage)
		if err != nil {
			t.Error(err)
		}
		if receivedMessage.Text != sentMessage.Message {
			t.Errorf("Received message content is not the same as sent message content")
		}
		fmt.Fprintln(w, "")
	}))
	defer server.Close()
	wh := New(server.URL)
	err := wh.Send(sentMessage, true)
	if err != nil {
		t.Error(err)
	}
}
